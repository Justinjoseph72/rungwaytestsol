package com.justin.app.repos;

import com.justin.app.domain.Person;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;

public interface PersonRepo  extends GraphRepository<Person> {

    @Query("MATCH (P:Person)\n" +
            "WITH P, rand() AS number\n" +
            "RETURN P\n" +
            "ORDER BY number\n" +
            "LIMIT 1")
    Person getRandomPerson();
}
