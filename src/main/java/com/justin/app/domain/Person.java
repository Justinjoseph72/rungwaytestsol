package com.justin.app.domain;


import lombok.Getter;
import lombok.Setter;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@NodeEntity
public class Person {

    @GraphId
    private Long id;

    @Getter
    @Setter
    private String name;

    public Person(String name) {
        this.name = name;
    }

    public Person(){

    }

    @Relationship(type = "FRIENDSHIP",direction = Relationship.UNDIRECTED)
    public Set<Friendship> friendships;

    public void friendsWith(String name, LocalDate since){
        if(friendships==null){
            friendships = new HashSet<>();
        }
        Friendship friendship = new Friendship(name,since);
        friendships.add(friendship);
    }
}
