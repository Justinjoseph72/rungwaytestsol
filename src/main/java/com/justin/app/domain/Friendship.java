package com.justin.app.domain;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

public class Friendship {

    @Getter
    @Setter
    private String friendName;

    @Getter
    @Setter
    private LocalDate startDate;

    public Friendship(){

    }

    public Friendship(String friendName,LocalDate startDate){
        this.friendName = friendName;
        this.startDate = startDate;
    }

}
