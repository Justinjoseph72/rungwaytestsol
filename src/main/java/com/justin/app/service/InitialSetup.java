package com.justin.app.service;


import com.justin.app.domain.Person;
import com.justin.app.repos.PersonRepo;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

public class InitialSetup {
    Logger logger = LoggerFactory.getLogger(InitialSetup.class);

    @Autowired
    PersonRepo personRepo;
    List<Person> personList = new ArrayList<>();
    @PostConstruct
    public void setup(){

        reset();
        populateDatabase();
        logger.info(String.valueOf(personList.size()));
        personRepo.save(personList);

        Person p = findRandomPerson();
        logger.info(p.getName());


    }

    private void populateDatabase() {
      for(int i=0;i<1000;i++){
          String randomName = RandomStringUtils.randomAlphabetic(6);
         Person person =new Person(randomName);
          personList.add(person);

      }
    }

    private void reset(){
        personRepo.deleteAll();
        logger.debug("All data from the database is cleared");

    }

    public Person findRandomPerson(){

        Person person = personRepo.getRandomPerson();
        return person;
    }
}
